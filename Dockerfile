FROM python:3.9-buster
 
COPY . /kockatoo-server

COPY users.pickle /users.pickle

RUN cd kockatoo-server

RUN pip install -r kockatoo-server/requirements.txt

EXPOSE 8000

CMD python kockatoo-server/main.py