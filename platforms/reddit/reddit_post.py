import json
import praw
import requests
import time
import os 

def log_print(x):
    print ("["+time.strftime("%H:%M:%S")+"] "+x)

credentials = os.environ['CREDS_PATH'] + '/client_secrets.json'

def reddit_post(subr,title,subtext):
    with open(credentials) as f:
            creds = json.load(f)
    
    reddit = praw.Reddit(client_id     = creds['client_id'],
                         client_secret = creds['client_secret'],
                         user_agent    = creds['user_agent'],
                         redirect_uri  = creds['redirect_uri'],
                         refresh_token = creds['refresh_token']
    )
    
    subr = subr
    subreddit = reddit.subreddit(subr)
    title = title

    log_print(f"Posting to {subreddit} with title {title} and subtext {subtext}")
    response = subreddit.submit(title, subtext)
    return response
