import tweepy
import time
import json
import pickle
import os 

creds_path = os.environ['CREDS_PATH'] + '/twitter.pickle'

def log_print(x):
    print ("["+time.strftime("%H:%M:%S")+"] "+x)

def tweet(text,image = '',schedule = ' '):
    with open(creds_path,'rb') as f:
        twitter_auth_keys = pickle.load(f)

    auth = tweepy.OAuthHandler(
            twitter_auth_keys['consumer_key'],
            twitter_auth_keys['consumer_secret']
    )

    auth.set_access_token(
            twitter_auth_keys['access_token'],
            twitter_auth_keys['access_token_secret']
    )

    api = tweepy.API(auth)
    print (image)
    if image == '':
         tweet = text
         log_print("Posting status "+" " + text + "to twitter")
         response = api.update_status(status=tweet)
    else : 
        log_print("Trying to post Media to twitter")
        log_print(f"Media selected {image}")
        media_dict = api.media_upload(image)
        log_print("Media successfully uploaded to twitter")
        log_print("Media ID is ")
        log_print("Response: ")
        log_print(str(media_dict))
        log_print("Media successfully uploaded to twitter")
        tweet = text
        response = api.update_status(status=tweet, media_ids=[media_dict.media_id])
    return response._json['id']

def twitter_auth():
    with open(creds_path, 'rb') as f:
        twitter_auth_keys = pickle.load(f)
    
    auth = tweepy.OAuthHandler(
            twitter_auth_keys['consumer_key'],
            twitter_auth_keys['consumer_secret']
    )
    auth.set_access_token(
            twitter_auth_keys['access_token'],
            twitter_auth_keys['access_token_secret']
    )
    api = tweepy.API(auth)
    try:
        for status in tweepy.Cursor(api.home_timeline).items(200):
            return 1
    except:
        tweepy.TweepError
        return 0
