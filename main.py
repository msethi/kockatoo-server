from fastapi import FastAPI,Depends,HTTPException, status
from fastapi.openapi.utils import get_openapi
from fastapi.security import OAuth2PasswordBearer,OAuth2PasswordRequestForm
import uvicorn
from jose import JWTError, jwt

from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.schedulers import SchedulerAlreadyRunningError
from datetime import datetime,timedelta
from pydantic import BaseModel
from typing import Optional

from platforms.mastodon_post.mastodon_post import toot, mastodon_auth
from platforms.twitter.twitter_post import tweet, twitter_auth
from platforms.reddit.reddit_post import reddit_post

from utils import User,Token,UserInDB
from utils import pwd_context,oauth2_scheme
from utils import verify_password,get_user,authenticate_user,create_access_token,get_current_user
from secrets import users,master_key,salt,SECRET_KEY,ALGORITHM,ACCESS_TOKEN_EXPIRE_MINUTES


tags_metadata = [
    {
        "name": "post",
        "description": "The main posting api endpoint. Don't forget to call /sched/start to start the scheduler",
    },
    {
        "name": "sched",
        "description": "To interface and play around with the scheduler",
    },
]

app = FastAPI(title="Kockatoo",
    description="Kockatoo lets you post to multiple social media sites at once",
    version="alpha",openapi_tags=tags_metadata)

sched = BackgroundScheduler()

@app.get("/post/{platform}/", tags=["post"])
async def post(platform,image: str = '', text: str = '', time: str = '', mastodon_base_url: str = '', subr: str = '', reddit_text: str = '', 
               reddit_heading: str = '',token: str = Depends(oauth2_scheme)):
        # Set default post times to 2 minutes 
        time = time or datetime.now() + timedelta(minutes=2)
        # Create a dictionary of the jobs, using lambda not to add jobs on dictionary evaluation
        potential_jobs_ids = {
            'twitter': lambda: sched.add_job(tweet, 'date', run_date=time,args=[text, image]).id,
            'reddit': lambda: sched.add_job(reddit_post, 'date', run_date=time, args=[subr, reddit_heading, reddit_text]).id,
            'mastodon': lambda: toot_job = sched.add_job(toot, 'date', run_date=time, args=[text, mastodon_base_url, image]).id
        }
        actual_jobs_ids = {'Details': "Start the scheduler using /sched/start to start the jobs"}
        if platform in ('all', 'twitter'):
            actual_jobs_ids['tweet_id'] = potential_jobs_ids['twitter']()
        if platform in ('all', 'reddit'):
            actual_jobs_ids['reddit_id'] = potential_jobs_ids['reddit']()
        if platform in ('all', 'mastodon'):
            actual_jobs_ids['mastodon_id'] = potential_jobs_ids['mastodon']()
        return actual_jobs_ids

@app.get("/")
async def root():
    return {"Output":"Welcome to Kockatoo Server, head to /docs or /redoc for more info"}

@app.get("/ping")
async def ping():
    return {"Output":"pong"}

@app.get("/auth_test")
async def auth_test(api_base_url):
    if api_base_url == '':
        return {"Output":"Please enter a vaild api_base_url for mastodon"}
    mastodon_status = mastodon_auth(api_base_url)
    twitter_status = twitter_auth()
    auth_status = {
        'mastodon_status' : mastodon_auth(api_base_url),
        'twitter_status' : twitter_auth()
        }
    return {auth_status}

@app.get("/sched/{action}", tags=["sched"])
async def scheduler(action, id: str = ''):
    scheduled_jobs = {}
    if action == 'start':
        try:
            sched.start()
            return {"Output":"scheduler started"}
        except SchedulerAlreadyRunningError:
            return {"Output":"Scheduler is already running"}
    if action == 'pause':
        sched.pause()
        return {"Output":"scheduler paused succesfully"}
    if action == 'cancel':
        sched.remove_job(id)
        return {"Output":"Job remove successfully"}
    if action == 'remove_all':
        sched.remove_all_jobs()
        return {"Output":"All jobs removed"}
    if action == 'get':
        all_jobs = {}
        jobs = sched.get_jobs()
        for i in range(len(jobs)):
            details = {
                'id': jobs[i].id,
                'time': str(jobs[1].trigger)
            }
            all_jobs[jobs[i].name] = (details)
        return all_jobs

@app.get("/users/me")
async def read_users_me(current_user: User = Depends(get_current_user)):
    return current_user

@app.post("/token", response_model=Token)
async def login_for_access_token(form_data: OAuth2PasswordRequestForm = Depends()):
    user = authenticate_user(users, form_data.username, form_data.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = create_access_token(
        data={"sub": user.username}, expires_delta=access_token_expires
    )
    return {"access_token": access_token, "token_type": "bearer"}


if __name__ == "__main__":
    uvicorn.run("main:app", host="0.0.0.0", port=8000, log_level="info")
