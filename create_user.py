from main import get_password_hash
from getpass import getpass
from secrets import users,salt
from pathlib import Path
import pickle
import os 
user_path = Path.cwd()  / 'users.pickle'

def adduser():
    username = input("Enter Username: ")
    password = getpass("Enter Your password: ")
    full_name = input("Enter your Full Name: ")
    mail = input("Enter your email: ")
    users[username] = {
        "username": username,
        "full_name": full_name,
        "email": mail,
        "hashed_password": get_password_hash(password),
        "disabled": False,
    }
    with open(user_path, 'wb') as outfile:
        pickle.dump(users, outfile)
    print('User print successfuly')

adduser()
